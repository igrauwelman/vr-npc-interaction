08-03-2022
- created Unity project and Gitlab repository
- created bar for the scene
	-> decided to make it a "basement bar" so that I don't need to worry what should be seen outside the windows
	-> decided to use guitar/folk background music that I would expect to hear in a pub/casual bar; makes the bar scene more believable
- VR player cannot teleport behind the counter or outside the bar to keep it simple
- NPCs are modeled with 4 cubes each: a bigger one for the body, one for the head and 2 smaller ones each for the hands 
	-> decided to also model hands so that it is easier to let the bartender look busy / look like he is doing something; came in handy later as well for modeling the decoratives
- bartender idle animation: facing to the wall / player sees his back at first (according to implementation plan), looking slightly downwards at the worktop where his hands are animated in a simple way 
- decoratives: standing at the short side of the L-shaped countertop (according to implementation plan)
	-> one is animated with nodding and looking around once in a while (listener), the other one is gesturing something with their hands (speaker) while looking at the listener
 		-> decided for this roles for simpler animations
- decoratives are colored in gray shades (according to Sumin's suggestion), bartender's body is orange (t-shirt or similar) and head is brown-ish (hair)
- walls are in a somewhat darker blue shade because it is said to be calming (https://www.elledecor.com/design-decorate/color/a25781168/calming-colors/)
- bar countertop is in a darker green shade because it looked like it would catch my attention this way and we want the player to go near it to initate the interaction with the bartender
- also added a hanging tv to add to the pub/casual bar atmosphere

Remarks: 
- scene feels very empty for a bar right now because there are only 2 other customers
- I might add more objects to the wall, maybe something like a dartboard so that there is "something to do" (in principle)
- I might add a door or a room in the back where the kitchen can be added later (where Toto was working) 
	-> location for quest object? (so that the player can in principle guess where they should search for fulfilling the quest, event though they are not supposed to do so in the scope of this thesis)

09-03-2022
- added a trigger area in front of the bar counter; if the player enters, the bartender will initate the conversation
- for now, the bartender is animated to look shortly over his shoulder and then proceed to walk to the counter, greeting the player by waving 
	-> like this, the bartender will always stand at the same position when facing the player, thus not directly approaching where the player stands
- added dialogue boxes: 
	- when the bartender greets the player, a text box appears left to him, containing the conversation text ("Hi, is there anything I can do for you?")
	- shortly after, another box appears right to him, containing 3 buttons ("Do you know Toto?", "How are you?", "No, thank you.") that the player can select with the ray interactor (default XR implementation: ray for teleporting 		  and selecting, always on)
	- depending on which button the player chooses, the conversation text in the text box to the left will be updated; for now, alternative texts are implemented in case the player chooses a button for a second time
	- if the player chooses to ask about Toto, the bartender will go on telling him to search for the business card of his therapist that should be somewhere in the bar (not implemented); once he did so, a grey question mark appears 	  above the bartender's head for indicating an "open" quest
- added placeholder decorative NPC chatter

Remarks:
	- decorative NPC chatter is just a placeholder for now, I need to find a better one in the end
	- button texts need to be adjusted over the course of the conversation (e.g. the exit button still says "No, thank you" even after the bartender spoke about what happened the day before in the bar)
	- not sure how I feel about the question mark yet

10-03-2022
- added simple bartender animations during conversation (slight nodding, head shakes, attempting to search for the business card he is mentioning)
- for now, if player exits the conversation, they need to leave the trigger area and get in it again to trigger another conversation 
- if player selects another answer while the bartender is talking, the current dialogue is stopped and the new one is displayed

Remarks: 
	- add reactions to the player choosing another answer/button while the bartender is talking?
	- animations during conversation are very simple for now, maybe they need to be more advanced/fine-grained 
	- I need to think about how to allow the player to start another conversation without having to leave the trigger area (that the player generally does not know of); maybe "knocking" on the counter to let the bartender know that 		  they are still there? or let the bartender initate another conversation on his own if the player stays in the trigger area for a certain amount of time?

14-03-2022
- managed to make a first AI version work: bartender shortly looks over his right shoulder if player enters trigger area and then finds its way to the counter via NavMesh; also follows the player if they change the position at the counter
- added an empty parent to the bartender NPC so that the animations made on the bartender NPC object are not bound to the position they were created at

Remarks: 
	- does not look very humanoid yet
	- previous animations not yet working, but shouldn't be that difficult

16-03-2022
- remade the animations for the conversation
- once the player enters the trigger area, the teleportation is disabled until they leave the conversation again -> easier to manage for bartender movement
- bartender movement still not perfect, but finally decently working

Remarks: 
	- bartender is standing too close to the counter right now when talking to the player -> planned to adjust tomorrow
	- not sure if I like the teleportation being disabled during conversation; definitely makes it easier to manage

17-03-2022
- adjusted the animations again (smoother transitions)
- added animation rigging for the decorative NPCs: if the player enters a trigger area near them, they will look over to the player after a short amount of time (without breaking their conversation animations)
- first attempts at making it possible to trigger the conversation again without the player having to leave the trigger area -> not perfectly working yet (animations are off before approaching the player again)
- adjusted the buttons during conversation

Remarks: 
	- should the decorative NPCs talk to the player if they are standing next to them for too long? (for now: only looking at the player while remaining in conversation)
	- bartender rotation still not finalized, but in a decent state for now

19-03-2022
- received review from Sascha: bartender movement not necessary to be advanced, simple version is okay for now - UI facing the player more important; dialogue manager not scalable as it was, should be used for every dialogue in Toto's watch; general script for NPCs with states like working, talking idle etc.
- worked on the main branch again for now (AI version since 14-03-2022 is on a separate branch):
	- UI faces the player
	- created a dialogue system -> all texts are set in the editor
	- created an abstract class for NPCs with subclasses BartenderNPC and DecorativeNPC
	- copied the disable-teleportation-during-conversation functionality from the other branch
	- copied the bartender-approaching-player-again-if-they-stay-in-the-trigger-area functionality but noticed that it does not work as it should (even if player is not in trigger area still, the bartender will initate a conversation 	again)

Remarks:
	- need to find a way to incorporate the animations during conversation again
	- refine second conversation trigger (maybe think about another solution than checking if the player stays there)
	- merge decorative NPCs looking at player into main (this) branch (animation rigging)

21-03-2022
- decoratives look at player again if near enough for some time
- added a system to match animations to the displayed sentences (animations are the same as in the AI branch now)
- button texts for Toto button and Mood button are updated during conversation
- made question mark yellow instead of gray so it is better visible (signal color, but not red because this would seem to me to signal something bad like a warning)

Remarks: 
	- removed trigger stay attempts for now because they do not work properly

24-03-2022
- received feedback from Sumin: merge both versions / add AI movement to refined simple version; make bar counter darker; make it more clear whose business card is spoken about
- added NavMeshAgent, simpler movement for now than on the AI branch -> if player goes behind the bartender to the counter, AI movement is buggy, going back to work not working properly
- made the bar counter top darker

Remarks:
	- only thing not working is bartender going back to work if the player went to the bar really close to the bartender
	- second conversation trigger will be left out for now
	- I want to add some comments to the code so that it is easier to follow

25-03-2022
- added comments and cleaned up project according to LobVR dev conventions
- bartender movement is not perfect now, but works

------- Version 1 ready for testing -------