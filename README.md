# VR-NPC Interaction

This project is part of my Bachelor thesis at the University of Osnabrück and LobVR, supervised by Prof. Dr. phil. Kai-Uwe Kühnberger ([IKW Osnabrück](https://www.ikw.uni-osnabrueck.de/en/home.html)) and Sumin Kim, M. Sc. ([LobVR](https://www.lobvr.com/)). The aim was to investigate important criteria for player-NPC interactions to derive assumptions about their importance in a VR context. The implementation of this prototype was guided by LobVR's project and previous work in the literature.

### External assets


- Background music: ["Melody loop 120 bpm"](https://freesound.org/people/DaveJf/sounds/592166/) by _DaveJf_ (last accessed on June 18, 2022 through [Freesound](https://freesound.org/))
- Decorative NPC's chatter: ["ShepherdsStory.mp3"](https://freesound.org/people/acclivity/sounds/211669/) by _acclivity_ (last accessed on June 18, 2022 through [Freesound](https://freesound.org/))
