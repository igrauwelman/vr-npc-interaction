using UnityEngine;

namespace Prototyping.Code.UI
{
    /// <summary>
    /// Lets UI elements face the player
    /// </summary>
    public class UILookAtPlayer : MonoBehaviour
    {
        public void Update()
        {
            Camera camera = Camera.main;
            transform.LookAt(new Vector3(camera.transform.position.x, transform.position.y, camera.transform.position.z), Vector3.up);
            transform.Rotate(0,180,0);
        }
    }
}