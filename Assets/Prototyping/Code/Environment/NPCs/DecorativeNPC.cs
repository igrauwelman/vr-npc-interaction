using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Animations.Rigging;
using Random = UnityEngine.Random;

namespace Prototyping.Code.Environment.NPCs
{
    public class DecorativeNPC : NPC
    {
        private bool _lookAtPlayer;
        [SerializeField] private MultiAimConstraint _headConstraint;
    
        private void Awake()
        {
            if (_headConstraint == null)
            {
                Debug.Log("head constraint not assigned on " + this.gameObject.name);
            }
        
            _headConstraint.weight = 0f;
            _lookAtPlayer = false;
        }
    
        void Start()
        {
            _state = State.TALKING;
        }
    
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                StartCoroutine(WaitAndLookAtPlayer());
            }
        }
    
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _headConstraint.weight = 0f;
                _lookAtPlayer = false;
            }
    
        }
    
        private IEnumerator WaitAndLookAtPlayer()
        {
            yield return new WaitForSeconds(Random.Range(2.5f, 5f));
    
            _lookAtPlayer = true;
        }
    
        private void Update()
        {
            if (_lookAtPlayer)
            {
                // smoothly increase weight until weight is 1
                if (Math.Abs(_headConstraint.weight - 1) > 0.1)
                {
                    _headConstraint.weight += 0.5f * Time.deltaTime;
                }
            }
            else
            {
                // smoothly decrease weight until weight is 0 again
                if (Math.Abs(_headConstraint.weight - 0) > 0.1)
                {
                    _headConstraint.weight -= 1f * Time.deltaTime;
                }
            }
        }
    }
}
