using UnityEngine;

namespace Prototyping.Code.Environment.NPCs
{
    public abstract class NPC : MonoBehaviour
    {
        protected State _state;
        public State state
        {
            set => _state = value;
            get => _state;
        }
        public enum State
        {
            WORKING,
            TALKING,
        }

        public void UpdateState(State newState)
        {
            _state = newState;
        }
    }
}
