using System;
using System.Collections;
using Prototyping.Code.Environment.Dialogues;
using Prototyping.Code.Tools;
using UnityEngine;
using UnityEngine.AI;

namespace Prototyping.Code.Environment.NPCs
{
    public class BartenderNPC : NPC
    {
        [SerializeField] private GameObject _questMark;
        [SerializeField] private DialogueTrigger _dialogueTriggerScript;

        public Substate[] _animationsToto1;
        public Substate[] _animationsToto2;
        public Substate[] _animationsMood1;
        public Substate[] _animationsMood2;

        private Animator _animator;
        private Substate _substate;
        private NavMeshAgent _agent;
        private GameObject _player;
        private Transform _startTransformWorld; // start transform of parent object
        private Transform _startTransformLocal; // start transform of this object (bartenderNPC)
        private bool _simpleTurn; // if player goes to bar behind the bartender (plain AI movement not working properly)
        private bool _shouldGoBackToWork;
        private bool _shouldMoveToPlayer;

        public bool ShouldMoveToPlayer
        {
            set => _shouldMoveToPlayer = value;
        }

        public enum Substate
        {
            NODDING,
            SEARCHING,
            SHAKINGHEAD,
            TILTINGHEAD,
            IDLE,
        }

        private void Awake()
        {
            if (_questMark == null)
            {
                Debug.Log("question mark object not assigned on " + this.gameObject.name);
            }

            if (_dialogueTriggerScript == null)
            {
                Debug.Log("DialogueTrigger script not assigned on " + this.gameObject.name);
            }

            _animator = GetComponent<Animator>();
            _agent = GetComponent<NavMeshAgent>();
            _player = GameObject.Find("Player");
            _shouldMoveToPlayer = false;
            _shouldGoBackToWork = false;
            _simpleTurn = false; 
            _startTransformWorld = transform.parent.transform;
            _startTransformLocal = transform;
        }

        private void Start()
        {
            _state = State.WORKING;
        }

        public void GoBackToWork()
        {
            StartCoroutine(SayGoodbyeAndGoBackToWork());
        }

        private IEnumerator SayGoodbyeAndGoBackToWork()
        {
            _shouldMoveToPlayer = false;

            yield return new WaitForSeconds(1f);

            if (new CheckIfAnimatorParameterExists().Check(_animator, "greetPlayer"))
            {
                _animator.SetTrigger("greetPlayer");
            }

            yield return new WaitForSeconds(3f);

            _shouldGoBackToWork = true;

            yield return new WaitForSeconds(2f);

            UpdateState(State.WORKING);
            _animator.Play("working");
        }

        public void SetQuestMarkActive()
        {
            _questMark.SetActive(true);
        }

        /// <summary>
        /// Calls PlayAnimation with correct animation based on selected button
        /// (accessed via ButtonInfo script) and sentence (accessed via sentenceCounter)
        /// </summary>
        /// <param name="buttonInfo"></param> 
        /// <param name="sentenceCounter"></param>
        public void HandleAnimations(ButtonInfo buttonInfo, int sentenceCounter)
        {
            switch (buttonInfo.buttonIdentifier)
            {
                case "toto":
                    if (buttonInfo.ClickCounter < 1)
                    {
                        PlayAnimation(_animationsToto1[sentenceCounter]);
                    }
                    else
                    {
                        PlayAnimation(_animationsToto2[sentenceCounter]);
                    }

                    break;
                case "mood":
                    if (buttonInfo.ClickCounter < 1)
                    {
                        PlayAnimation(_animationsMood1[sentenceCounter]);
                    }
                    else
                    {
                        PlayAnimation(_animationsMood2[sentenceCounter]);
                    }

                    break;
            }
        }

        /// <summary>
        /// Plays animation based on substate
        /// </summary>
        /// <param name="substate"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        private void PlayAnimation(Substate substate)
        {
            switch (substate)
            {
                case Substate.NODDING:
                    _animator.Play("nodding");
                    break;
                case Substate.IDLE:
                    _animator.Play("idle");
                    break;
                case Substate.SEARCHING:
                    _animator.Play("searching");
                    break;
                case Substate.SHAKINGHEAD:
                    _animator.Play("shaking_head");
                    break;
                case Substate.TILTINGHEAD:
                    _animator.Play("tilting_head");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(substate), substate, null);
            }
        }

        private void Update()
        {
            if (_shouldMoveToPlayer)
            {
                _agent.isStopped = false;
                _agent.updateRotation = true;
                _shouldGoBackToWork = false;
                
                // in case player goes to bar somewhat further away (simple AI movement works well)
                if (Vector3.Distance(transform.parent.transform.position, _player.transform.position) >= 2.9f)
                {
                    _simpleTurn = false;
                    GoToTarget(true, _player.transform.position);
                }
                // in case player goes to bar behind the bartender (simple AI movement not working properly)
                else
                {
                    _simpleTurn = true;
                    TurnToPlayer();

                    // if bartender is rotated enough, go to player
                    if (transform.parent.transform.rotation.y > 0.3f)
                    {
                        GoToTarget(false, _player.transform.position);
                    }
                }
            }

            if (_shouldGoBackToWork)
            {
                if (_simpleTurn)
                {
                    StartCoroutine(GoBackToStartPositionAndRotation());
                }
                else
                {
                    GoToTarget(true, _startTransformWorld.position);
                }
            }
        }

        /// <summary>
        /// Smooth LookAt function with player as target
        /// </summary>
        private void TurnToPlayer()
        {
            Vector3 direction = _player.transform.position - transform.parent.transform.position;
            direction.y = 0f;
            transform.parent.transform.rotation = Quaternion.RotateTowards(transform.parent.transform.rotation,
                Quaternion.LookRotation(direction), Time.time * 0.7f);
        }

        private void GoToTarget(bool shouldUpdateRotation, Vector3 targetLocation)
        {
            _agent.updateRotation = shouldUpdateRotation;
            _agent.SetDestination(targetLocation);
        }

        private IEnumerator GoBackToStartPositionAndRotation()
        {
            GoToTarget(false, _startTransformWorld.position);

            yield return new WaitForSeconds(1f);
            
            _agent.isStopped = true;
            ResetPositionAndLocation();
        }

        private void ResetPositionAndLocation()
        {
            transform.position = _startTransformLocal.position;
            Quaternion rotationWorld = Quaternion.Euler(new Vector3(0f, 0f, 0f));
            transform.parent.transform.rotation = rotationWorld;
            Quaternion rotationLocal = Quaternion.Euler(new Vector3(0f, -90f, 0f));
            transform.rotation = rotationLocal;
        }
    }
}
