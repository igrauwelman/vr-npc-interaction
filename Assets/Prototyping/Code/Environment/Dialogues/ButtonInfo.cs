using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Prototyping.Code.Environment.Dialogues
{
    /// <summary>
    /// Manages button info and updates to-be-triggered dialogue and answer option text
    /// </summary>
    public class ButtonInfo : MonoBehaviour
    {
        [Tooltip("Answer option that should be displayed after the first click")]
        [SerializeField] private string _secondText;
    
        public string buttonIdentifier;
        public UnityEvent triggeredFunctions;
        public int ClickCounter { get; set; }
    
        private int _minClickCounter;
    
        private void Awake()
        {
            if (_secondText == null)
            {
                Debug.Log("second text not assigned on " + this.gameObject.name);
            }
        
            ClickCounter = 0;
        }

        /// <summary>
        /// Removes previous onClick() function and adds a new one
        /// </summary>
        /// <param name="oldDialogueIdentifier"></param>
        /// <param name="newDialogueIdentifier"></param>
        public void UpdateToBeTriggeredDialogue(string oldDialogueIdentifier, string newDialogueIdentifier)
        {
            GetComponent<Button>().onClick.RemoveListener(() => FindObjectOfType<DialogueManager>().ShowDialogue(oldDialogueIdentifier));
            GetComponent<Button>().onClick.AddListener(() => FindObjectOfType<DialogueManager>().ShowDialogue(newDialogueIdentifier));
        }

        public void UpdateButtonText()
        {
            GetComponentInChildren<TMP_Text>().text = _secondText;
        }

        /// <summary>
        /// Call functions specified in the editor if button was clicked _minClickCounter times
        /// or more
        /// </summary>
        public void TryToCallFunctions()
        {
            if (ClickCounter >= _minClickCounter)
            {
                triggeredFunctions.Invoke();
            }
        }

        public void SetMinClickCounterForTriggeringFunctions(int minClickCounter)
        {
            _minClickCounter = minClickCounter;
        }
    }
}
