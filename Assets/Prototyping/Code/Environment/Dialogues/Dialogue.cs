using UnityEngine;

namespace Prototyping.Code.Environment.Dialogues
{
    [System.Serializable]
    public class Dialogue
    {
        [Tooltip("Has to correspond to buttonIdentifier with suffix 1")]
        public string dialogueIdentifier1;
    
        [Tooltip("Dialogue triggered after first click")]
        [TextArea(3, 10)]
        public string[] sentences1;

        [Tooltip("Has to correspond to buttonIdentifier with suffix 2")]
        public string dialogueIdentifier2;

        [Tooltip("Dialogue triggered after second click")]
        [TextArea(3, 10)] 
        public string[] sentences2;
    }
}
