using System.Collections;
using System.Collections.Generic;
using Prototyping.Code.Environment.NPCs;
using Prototyping.Code.Tools;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

namespace Prototyping.Code.Environment.Dialogues
{
    /// <summary>
    /// Triggers the dialogue with the specified NPC if player enters the trigger area
    /// </summary>
    public class DialogueTrigger : MonoBehaviour
    {
        public List<Dialogue> dialogues;
        public UnityEvent endDialogue;

        [SerializeField] private Animator _animator;
        [SerializeField] private NPC _npc;
        [SerializeField] private List<GameObject> _teleportationAreas;
   
        private void Awake()
        {
            if (_animator == null)
            {
                Debug.Log("bartender animator not assigned on " + this.gameObject.name);
            }
           
            if (_npc == null)
            {
                Debug.Log("npc not assigned on " + this.gameObject.name);
            }
           
            if (_teleportationAreas == null)
            {
                Debug.Log("teleportation areas not assigned on " + this.gameObject.name);
            }
        }
   
        private void OnTriggerEnter(Collider other) 
        { 
            if (other.CompareTag("Player"))
            {
                DisableTeleportation();
                StartCoroutine(GreetAndStartConversation());
            }
        }

        private IEnumerator GreetAndStartConversation()
        {
            if (new CheckIfAnimatorParameterExists().Check(_animator, "lookOverShoulder"))
            {
                _animator.SetTrigger("lookOverShoulder");
            }
       
            yield return new WaitForSeconds(2f);
       
            _npc.gameObject.GetComponent<BartenderNPC>().ShouldMoveToPlayer = true;
            if (new CheckIfAnimatorParameterExists().Check(_animator, "greetPlayer"))
            {
                _animator.SetTrigger("greetPlayer");
            }
       
            yield return new WaitForSeconds(2f);
       
            _npc.UpdateState(NPC.State.TALKING);
            TriggerDialogue();
        }

        private void TriggerDialogue()
        {
            FindObjectOfType<DialogueManager>().GetDialoguesAndStartConversation(dialogues, _npc);
        }
   
        private void DisableTeleportation()
        {
            GameObject.Find("Player").GetComponent<TeleportationProvider>().enabled = false;
            foreach (var area in _teleportationAreas)
            {
                area.GetComponent<TeleportationArea>().enabled = false;
            }
        }
   
        public void EnableTeleportation()
        {
            GameObject.Find("Player").GetComponent<TeleportationProvider>().enabled = true;
            foreach (var area in _teleportationAreas)
            {
                area.GetComponent<TeleportationArea>().enabled = true;
            }
        }
    }
}
