using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Prototyping.Code.Environment.NPCs;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Prototyping.Code.Environment.Dialogues
{
    /// <summary>
    /// Sets up, maintains and ends a dialogue
    /// </summary>
    public class DialogueManager : MonoBehaviour
    { 
        private Dictionary<string, string[]> _dialogueIdentifierAndSentences;
        private ButtonInfo _button;
        private int _conversationCounter;
        private BartenderNPC _bartender;

        [SerializeField] private TMP_Text _dialogueText;
        [SerializeField] private GameObject _dialogueBox;
        [SerializeField] private GameObject _answerBox;
        [SerializeField] private List<Button> _answerButtons;
        [SerializeField] private DialogueTrigger _dialogueTriggerScript;

        private void Awake()
        {
            if (_dialogueText == null)
            {
                Debug.Log("dialogue text not assigned on " + this.gameObject.name);
            }
        
            if (_dialogueBox == null)
            {
                Debug.Log("dialogue box not assigned on " + this.gameObject.name);
            }
        
            if (_answerBox == null)
            {
                Debug.Log("answer box not assigned on " + this.gameObject.name);
            }
        
            if (_answerButtons == null)
            {
                Debug.Log("answer buttons not assigned on " + this.gameObject.name);
            }
        
            if (_dialogueTriggerScript == null)
            {
                Debug.Log("DialogueTrigger script not assigned on " + this.gameObject.name);
            }
        }

        private void Start()
        {
            _dialogueIdentifierAndSentences = new Dictionary<string, string[]>();
            _conversationCounter = 0;
        }

        /// <summary>
        /// Adds sentences (as specified in the editor) to the dictionary and starts the dialogue
        /// by making the UI elements visible and setting the greeting text
        /// </summary>
        /// <param name="dialogues"></param>
        /// <param name="npc"></param>
        public void GetDialoguesAndStartConversation(List<Dialogue> dialogues, NPC npc)
        {
            // check whether conversation partner is the bartender and store for later
            if (npc.GetComponent<BartenderNPC>() != null)
            {
                _bartender = (BartenderNPC) npc;
            }
        
            _conversationCounter += 1;
        
            foreach (var dialogue in dialogues)
            {
                _dialogueIdentifierAndSentences[dialogue.dialogueIdentifier1] = dialogue.sentences1;
                _dialogueIdentifierAndSentences[dialogue.dialogueIdentifier2] = dialogue.sentences2;
            }

            if (_conversationCounter <= 1)
            {
                SetDialogueIdentifiersOnButtons();
            }

            StartCoroutine(ShowDialogueBoxes());

            _dialogueText.text = _dialogueIdentifierAndSentences[("greeting" + Mathf.Clamp(_conversationCounter, 1f, 2f))][0];
        }

        /// <summary>
        /// Sets the function that the buttons' onClick() should trigger.
        /// IMPORTANT: For this to work, the button identifiers and the dialogue identifiers
        /// need to have the same name (without the number suffix for the latter).
        /// </summary>
        private void SetDialogueIdentifiersOnButtons()
        {
            foreach (var button in _answerButtons)
            {
                button.onClick.AddListener(() => ShowDialogue((button.GetComponent<ButtonInfo>().buttonIdentifier + "1").ToString()));
            }
        }

        private IEnumerator ShowDialogueBoxes()
        {
            _dialogueBox.SetActive(true);

            yield return new WaitForSeconds(2f);
        
            _answerBox.SetActive(true);
        }

        private IEnumerator HideDialogueBoxes()
        {
            _answerBox.SetActive(false);

            yield return new WaitForSeconds(2f);
        
            _dialogueBox.SetActive(false);
            _dialogueText.text = _dialogueIdentifierAndSentences["greeting2"][0];
        }

        public void ShowDialogue(string dialogueIdentifier)
        { 
            StopAllCoroutines();
            _dialogueText.text = "...";

            // get the last selected button (if triggered by button click)
            if (EventSystem.current.currentSelectedGameObject != null && GameObject.Find(EventSystem.current.currentSelectedGameObject.name) != null)
            {
                _button = GameObject.Find(EventSystem.current.currentSelectedGameObject.name).GetComponent<ButtonInfo>();
            }
        
            var sentences = _dialogueIdentifierAndSentences[dialogueIdentifier];
            StartCoroutine(ShowSentencesWithDelayAndUpdateButton(sentences, _button != null ? _button : null));

            if (dialogueIdentifier.Contains("exit"))
            {
                EndDialogue();
            }
        }

        /// <summary>
        /// Displays the sentences, calls functions the button should trigger (if there are any)
        /// and updates the button text as well as the to-be-triggered dialogue
        /// </summary>
        /// <param name="sentences"></param>
        /// <param name="buttonInfoScript"></param>
        /// <returns></returns>
        private IEnumerator ShowSentencesWithDelayAndUpdateButton(string[] sentences, ButtonInfo buttonInfoScript)
        {
            yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
        
            // loop through the sentences and display them 
            // if NPC is the bartender, play corresponding animations
            for (var i = 0; i < sentences.Length-1; i++)
            {
                _dialogueText.text = sentences[i];
            
                if (_bartender != null)
                {
                    _bartender.HandleAnimations(buttonInfoScript, i);
                }

                yield return new WaitForSeconds(4f);
            }

            // (last sentence outside the loop because no delay after it is needed)
            _dialogueText.text = sentences[sentences.Length - 1];
        
            if (_bartender != null)
            {
                _bartender.HandleAnimations(buttonInfoScript, sentences.Length - 1);
            }

            if (buttonInfoScript)
            {
                buttonInfoScript.ClickCounter += 1;
                buttonInfoScript.TryToCallFunctions();
                buttonInfoScript.UpdateButtonText();
            }
        
            UpdateButtonToBeTriggeredDialogue(_dialogueIdentifierAndSentences.FirstOrDefault(x => x.Value == sentences).Key);
        }

        /// <summary>
        /// Updates the to-be-triggered dialogue if current dialogue is the first version
        /// (dialogueIdentifier1)
        /// </summary>
        /// <param name="dialogueIdentifier"></param>
        private void UpdateButtonToBeTriggeredDialogue(string dialogueIdentifier)
        {
            if (dialogueIdentifier.Contains("1"))
            {
                if (EventSystem.current.currentSelectedGameObject != null)
                {
                    _button.UpdateToBeTriggeredDialogue(dialogueIdentifier, (dialogueIdentifier.Remove(dialogueIdentifier.Length-1) + "2").ToString());
                }
            }
        }

        private void EndDialogue()
        {
            StartCoroutine(HideDialogueBoxes());
            _dialogueTriggerScript.endDialogue.Invoke();
        }
    }
}
