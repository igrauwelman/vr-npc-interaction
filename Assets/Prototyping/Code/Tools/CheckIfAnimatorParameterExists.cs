using UnityEngine;

namespace Prototyping.Code.Tools
{
    /// <summary>
    /// Checks whether a given parameter exists in a given animator
    /// </summary>
    public class CheckIfAnimatorParameterExists
    {
        public bool Check(Animator animator, string parameter)
        {
            foreach (var param in animator.parameters)
            {
                if (param.name == parameter)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
